"use strict";

/* Task #1 */

const encryptDecrypt = require('../utils/xorEncrypt');
const fromHexToDecimal = require('../utils/toDecimal');

const cipherText = '7958401743454e1756174552475256435e59501a5c524e176f786517545e475f5245191772195019175e4317445f58425b531743565c521756174443455e595017155f525b5b58174058455b5315175659531b17505e41525917435f52175c524e175e4417155c524e151b174f584517435f5217515e454443175b524343524517155f1517405e435f17155c151b17435f5259174f58451715521517405e435f171552151b17435f525917155b1517405e435f17154e151b1756595317435f5259174f58451759524f4317545f564517155b1517405e435f17155c15175650565e591b17435f52591715581517405e435f171552151756595317445817585919176e5842175a564e17424452175e5953524f1758511754585e59545e53525954521b177f565a5a5e595017535e4443565954521b177c56445e445c5e17524f565a5e5956435e58591b17444356435e44435e54565b17435244434417584517405f564352415245175a52435f5853174e5842175152525b174058425b5317445f584017435f52175552444317455244425b4319';
const decimalCipherText = fromHexToDecimal(cipherText.replace(/\s/g, ''));

for (let i = 0; i < 256; i++) {
  console.log(`-----------------------------------${String.fromCharCode(i)}-----------------------------------`);
  console.log(encryptDecrypt(decimalCipherText, String.fromCharCode(i)));
  console.log(`-------------------------------------------------------------------------`);
}

/*
  Result:
  Now try a repeating-key XOR cipher. E.g. it should take a string "hello world" and,
  given the key is "key", xor the first letter "h" with "k", then xor "e" with "e", then
  "l" with "y", and then xor next char "l" with "k" again, then "o" with "e" and so on.
  You may use index of coincidence, Hamming distance, Kasiski examination, statistical
  tests or whatever method you feel would show the best result.
*/