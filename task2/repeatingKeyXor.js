"use strict";

/* Task #2 */

const encryptDecrypt = require('../utils/xorEncrypt');
const fromHexToDecimal = require('../utils/toDecimal');

const cipherText = '1b420538554c2d100f2354096c44036c511838510f27101f235d096c43052140 0029101f39521f385918394405235e4c2f591c24551e62103823101e2954192 f554c3858096c530321400029480538494c23564c3858053f100322554c3b55 4c3b59002010193f554c235e003510193c40093e530d3f554c20551838551e3 f1c4c3f5f4c3858096c5b0935431c2d53096c591f6c5f0220494c7e064d6c64 036c570938101824591f6c5f0229101e25570438100d39440321511825530d2 05c156c490339101b255c006c401e23520d2e5c156c5e0929544c385f4c3943 096c430321554c3f5f1e3810032a100b295e0938590f6c51002b5f1e2544042 11c4c3f5901395c0d3855086c510222550d2059022b10033e100b3e51082555 0238100829430f295e1862103f29420523451f2049406c471e2544096c59186 c42052b58186c5e033b1c4c355f196c4705205c4c2255092810053810182310 082953053c58093e101824554c22551438100322554c2d434c3b5500201e4c0 e550d3e1005221001255e0860101824551e29171f6c5e036c431c2d53093f1e';
const decimalCipherText = fromHexToDecimal(cipherText.replace(/\s/g, ''));

function shiftLeft(arr, pos) {
  if ((pos > arr.length) || (pos <= 0)) {
    throw new Error(`Bad value for shift: ${pos}`);
  }
  return arr.slice(pos).concat(arr.slice(0, pos))
}

function calcCoincidence(initArr, shiftedArr) {
  const len = initArr.length;
  let sum = 0;
  for(let i = 0; i < len; i++) {
    if (initArr[i] === shiftedArr[i]) {
      sum++;
    }
  }
  return sum / len;
}

function defineKeyLength(cipherArr) {
  const arrOfChars = cipherArr.split('');
  const keyLengths = [];
  console.log('Coincidence table:');
  for (let i = 1; i <= 15; i++) {
    const shiftedArr = shiftLeft(arrOfChars, i);
    const result = calcCoincidence(arrOfChars, shiftedArr);
    console.log(`i=${i} -> ${result}`);
    if (result > 0.055) {
      keyLengths.push(i);
    }
  }
  return keyLengths;
}

function divideByGroups(cipherArr) {
  const keyLengths = defineKeyLength(cipherArr);
  if (keyLengths.length === 0) {
    throw new Error('Array with key length is empty');
  }
  const keyLength = keyLengths[0];
  const groups = {};
  for (let i = 0; i < cipherArr.length; i++) {
    if (!groups [i % keyLength]) {
      groups[i % keyLength] = [];
    }
    groups[i % keyLength].push(cipherArr[i]);
  }
  return groups;
}

function findKey(groups) {
  const maxKeys = [];
  for (let key in groups) {
    const symbolsCounts = {};
    const group = groups[key];
    for (let i = 0; i < group.length; i++) {
      if (!symbolsCounts[group[i]]) {
        symbolsCounts[group[i]] = 1;
      } else {
        symbolsCounts[group[i]]++;
      }
    }
    let max = -100500;
    let maxKey;
    for (let symb in symbolsCounts) {
      max = max < symbolsCounts[symb] ? symbolsCounts[symb] : max;
      if (max === symbolsCounts[symb]){
        maxKey = symb;
      }
    }
    maxKeys.push(maxKey);
  }
  return maxKeys;
}

function getShiftedKey(maxKeys) {
  const keys = [];
  for (let i = 0; i < maxKeys.length; i++) {
    keys.push(maxKeys[i].charCodeAt(0) ^ ' '.charCodeAt(0));
  }
  return keys;
}

const groups = divideByGroups(decimalCipherText);

console.log(groups);

const maxKeys = findKey(groups);

console.log(maxKeys);

const keys = getShiftedKey(maxKeys);

console.log(keys);

const finalKey = keys.map(c => String.fromCharCode(c)).join('');

console.log(finalKey);

console.log(encryptDecrypt(decimalCipherText, finalKey));

/*
  Result:
  Write a code to hack some simple substitution cipher.
  To reduce the complexity of this one we will use only
  uppercase letters, so the keyspace is only 26! To get
  this one right automatically you will probably need to
  use some sort of genetic algorithm, simulated annealing or
  gradient descent. Seriously, write it right now, you will
  need it to decipher the next one as well. Bear in mind,
  there's no spaces.
* */