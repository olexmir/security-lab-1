class HackTask3 {
  constructor(text, trigrams) {

    this.text = text;

    this.quality = -100000;

    this.trigrams = trigrams;

    this.key = new Map([
      ["A", "A"], ["B", "B"], ["C", "C"], ["D", "D"],
      ["E", "E"], ["F", "F"], ["G", "G"], ["H", "H"],
      ["I", "I"], ["J", "J"], ["K", "K"], ["L", "L"],
      ["M", "M"], ["N", "N"], ["O", "O"], ["P", "P"],
      ["Q", "Q"], ["R", "R"], ["S", "S"], ["T", "T"],
      ["U", "U"], ["V", "V"], ["W", "W"], ["X", "X"],
      ["Y", "Y"], ["Z", "Z"]
    ]);

    this.alphabet = [
      "A", "B", "C", "D", "E", "F", "G",
      "H", "I", "J", "K", "L", "M", "N",
      "O", "P", "Q", "R", "S", "T", "U",
      "V", "W", "X", "Y", "Z"
    ];

  }

  showKey() {
    const result = [];
    this.key.forEach((value, key) => result.push({key, value}));
    return result;
  }

  dectypt(text, key) {
    let decryptedText = '';
    for (let i = 0; i < text.length; i++) {
      decryptedText += key.get(text.charAt(i));
    }
    return decryptedText;
  }

  calcQuality(text) {
    let quality = 0;
    for (let i = 0; i < text.length - 2; i++) {
      const trigram = text.substring(i, i + 3);
      if (this.trigrams.has(trigram)) {
        quality += this.trigrams.get(trigram);
      }
    }
    return quality;
  }

  swapKey() {
    const key = new Map(this.key);
    let pos1 = Math.floor(Math.random() * 26);
    let pos2 = Math.floor(Math.random() * 26);

    let a = key.get(this.alphabet[pos2]);
    let b = key.get(this.alphabet[pos1]);

    key.set(this.alphabet[pos1], a);
    key.set(this.alphabet[pos2], b);
    return key;
  }

  hack() {
    let result = '';

    let i = 0;

    while (i < 2000) {
      let currentKey = this.swapKey();
      let currentResult = this.dectypt(this.text, currentKey);
      let currentQuality = this.calcQuality(currentResult);
      if (currentQuality > this.quality) {
        this.quality = currentQuality;
        this.key = currentKey;
        result = currentResult;
        i = 0;
      }
      i++;
    }
    return result;
  }
}

module.exports = HackTask3;
