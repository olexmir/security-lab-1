"use strict";

/* Task #3 */

const fs = require('fs');

const trigrams = new Map();

let counter = 0;

fs.readFileSync('./trigrams.txt').toString()
  .split('\r\n')
  .forEach(row => {
    const parts = row.split(' ');
    const trigram = parts[0];
    const quantity = parts[1];
    if (!trigrams.has(trigram)) {
      trigrams.set(trigram, +quantity);
      counter += +quantity;
    }
  });

trigrams.forEach((value, key) => {
  return trigrams.set(key, Math.log10(value / counter));
});

const HackTask3 = require('./hack');

const data = "EFFPQLEKVTVPCPYFLMVHQLUEWCNVWFYGHYTCETHQEKLPVMSAKSPVPAPVYWMVHQLUSPQLYWLASLFVWPQLMVHQLUPLRPSQLULQESPBLWPCSVRVWFLHLWFLWPUEWFYOTCMQYSLWOYWYETHQEKLPVMSAKSPVPAPVYWHEPPLUWSGYULEMQTLPPLUGUYOLWDTVSQETHQEKLPVPVSMTLEUPQEPCYAMEWWYOYULULTCYWPQLSEOLSVOHTLUYAPVWLYGDALSSVWDPQLNLCKCLRQEASPVILSLEUMQBQVMQCYAHUYKEKTCASLFPYFLMVHQLUPQVSHEUEDUEHQBVTTPQLVWFLRYGMYVWMVFLWMLSPVTTBYUNESESADDLSPVYWCYAMEWPUCPYFVIVFLPQLOLSSEDLVWHEUPSKCPQLWAOKLUYGMQEUEMPLUSVWENLCEWFEHHTCGULXALWMCEWETCSVSPYLEMQYGPQLOMEWCYAGVWFEBECPYASLQVDQLUYUFLUGULXALWMCSPEPVSPVMSBVPQPQVSPCHLYGMVHQLUPQLWLRPHEUEDUEHQMYWPEVWSSYOLHULPPCVWPLULSPVWDVWGYUOEPVYWEKYAPSYOLEFFVPVYWETULBEUF";


for (let i = 0; i < 10; i++) {
  console.log(`Iteration #${i + 1}`);
  let hackTask3 = new HackTask3(data, trigrams);

  const result = hackTask3.hack();

  console.log('Final key:');
  hackTask3.showKey();

  console.log(result, '\n\n');

}


/*
Result

ADD THE ABILITY TO DECIPHER ANY KIND OF POLYALPHABETIC SUBSTITUTION CIPHERS THE ONE USED IN THE CIPHER
TEXTS HERE HAS TWENTY SIX INDEPENDENT RANDOMLY CHOSEN MONOALPHABETIC SUBSTITUTION PATTERNS FOR EACH LETTER
FROM ENGLISH ALPHABET IT IS CLEAR THAT YOU CAN NO MORE RELY ON THE SAME SIMPLE ROUTINE OF GUESSING THE
KEY BY EXHAUST I VE SEARCH WHICH YOU PROBABLY USED TO DECIPHER THIS PARAGRAPH WILL THE INDEX OF COINCIDENCE
STILL WORK AS A SUGGESTION YOU CAN TRY TO DIVIDE THE MESSAGE IN PARTS BY THE
NUMBER OF CHARACTERS IN A KEY AND APPLY FREQUENCY ANALYSIS TO EACH OF THEM CAN YOU FIND AWAY TO USE HIGHER
ORDER FREQUENCY STATISTICS WITH THIS TYPE OF CIPHER THEN EXTPA RAGRAPH CONTAINS SOME PRETTY INTERESTING
INFORMATION ABOUT SOME ADDITIONAL REWARD
* */





