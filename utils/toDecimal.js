"use strict";

module.exports = function fromHexToDecimal(hexString) {
  const splittedByTwo = hexString.match(/.{1,2}/g);
  const charCodes = splittedByTwo.map(hex => parseInt(hex, 16));
  const result = charCodes.map(e => String.fromCharCode(e)).join('');
  return result;
};
