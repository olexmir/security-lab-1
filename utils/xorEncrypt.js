"use strict";

module.exports = function encryptDecrypt(input, ikey) {
  const key = ikey.split('');
  const output = [];

  for (let i = 0; i < input.length; i++) {
    const charCode = input.charCodeAt(i) ^ key[i % key.length].charCodeAt(0);
    output.push(String.fromCharCode(charCode));
  }
  return output.join('');
};
